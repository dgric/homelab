#!/bin/sh

user='dg'
venv_path='/opt/ansible-venv'
ansible_repo='https://gitlab.com/dgric/homelab.git'
playbook='ansible/jumpbox.yml'

# Expire default user password
chage -d 0 "$user"

# Install ansible
mkdir -p "$venv_path"
python3 -m virtualenv "$venv_path"
. "$venv_path/bin/activate"

python3 -m pip install ansible

# Install and enable bootstrap.service
cat <<EOF >/etc/systemd/system/bootstrap.service
[Unit]
Description=First Boot Setup
After=network-online.target

[Service]
Type=oneshot
Environment="VIRTUAL_ENV=$venv_path"
Environment="PATH=$venv_path/bin:$PATH"
ExecStart=$venv_path/bin/ansible-pull -U $ansible_repo $playbook
ExecStartPost=/bin/systemctl disable bootstrap.service \
	; rm /etc/systemd/system/bootstrap.service \
	; /bin/systemctl reboot

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable bootstrap.service
